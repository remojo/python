# Imports ---------------

from guizero import App, TextBox, Drawing, Combo, Slider


# Functions -------------

def draw_meme():
    meme.clear()
    #here the original background has been replaced
    #its in the same directory as the app so it does
    #not need a full path reference.  
    meme.image(0, 0, "newb_newb2.png")
    meme.text(
        20, 20, top_text.value, 
        color=color.value,
        size=size.value, 
        font=font.value)
    meme.text(
        20, 470, bottom_text.value,
        color=color.value,
        #this will keep the top and bottom text the same size
        size=size.value,
        font=font.value,
        )


# App -------------------
#show off the name at the top
app = App("Noob-noob 9000")

top_text = TextBox(app, "I'm here for my first Python app!", command=draw_meme)
bottom_text = TextBox(app, "No thanks noob-noob.", command=draw_meme)

color = Combo(app,
              options=["black", "white", "red", "green", "blue", "orange"],
              command=draw_meme, selected="orange") #set the default value

font = Combo(app,
             options=["times new roman", "verdana", "courier", "impact"],
             command=draw_meme)

size = Slider(app, start=20, end=50, command=draw_meme)

meme = Drawing(app, width="fill", height="fill")

draw_meme()

app.display()
