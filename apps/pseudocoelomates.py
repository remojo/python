#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Synopsis of the Phyla of Metazoa
Source: Zoology:: Dorit, Walker, Barnes
ISBN 0-03-030504-7	Chapter 23, pg 555

Pseudocoelomates: Animals in which the blastocoel sometimes persists, 
                  forming a body cavity. 
                  Digestive tract with mouth and anus. 
                  Body usually covered with a cuticle.

Phylum Name (approx. number of species)

Phylum Gastrotricha (460)

Phylum Nematoda (12,000)

Phylum Nematomorpha (230)

Phylum Rotifera (230)

Phylum Acanthocephala (1,150)

Phylum Kinorhyncha (100)

Phylum Loricifera (1)

Phylum Priapulida (13)
"""

import matplotlib.pyplot as plt

pseudocoelomates_all = {'Nematoda': 12000, 'Acanthocephala': 1150, 
                      'Gastrotricha': 460, 'Nematomorpha': 230,
                      'Rotifera': 230, 'Kinorhyncha': 100, 
                      'Priapulida': 13, 'Loricifera': 1}
#create two lists from data dict
names_all = list(pseudocoelomates_all.keys())
values_all = list(pseudocoelomates_all.values())


pseudocoelomates_NO_nematoda = {'Acanthocephala': 1150, 'Gastrotricha': 460,
                                'Nematomorpha': 230, 'Rotifera': 230, 'Kinorhyncha': 100,
                                'Priapulida': 13, 'Loricifera': 1}

names_nn = list(pseudocoelomates_NO_nematoda.keys())    
values_nn = list(pseudocoelomates_NO_nematoda.values())


fig, axs = plt.subplots(2, 1, figsize=(9, 5))


axs[0].barh(names_all, values_all)
axs[0].set_xlabel('Including Nematodes', fontsize='large', fontweight='bold')

axs[1].barh(names_nn, values_nn)
axs[1].set_xlabel('Without Nematodes', fontsize='large', fontweight='bold')

plt.subplots_adjust(wspace=0, hspace=0.7)
fig.suptitle('Pseudocoelomates Species Counts', fontsize='x-large', fontweight='bold')

plt.grid()
plt.show()




