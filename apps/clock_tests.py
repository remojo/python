#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 12:00:33 2020
from Simon Monk, Raspberry Pi Cookbook 3rd Ed.


@author: tap3w0rm
"""

import time

def factorial(n):
    if n == 0:
        return 1
    else: 
        return n * factorial(n-1)
    
#measure the performance of your machine
#before_time = time.process_time()
before_time = time.perf_counter()

top = 10000000
fact = 200

for i in range(1, top):
    factorial(fact)

after_time = time.perf_counter()
#after_time = time.process_time()

print("Time to factorial {0} of {1}: ".format(fact, top), (after_time - before_time))
