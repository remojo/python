#!/usr/bin/env python3

'''
Code from Python GUI Book example from the
official raspberrypi.org site.
Supply your own base image, 
'''


# Imports ---------------


from guizero import App, TextBox, Drawing, Combo, Slider


# Functions -------------

def draw_meme():
    meme.clear()
   #replace the background image, has to be in same folder as this app 
    meme.image(0, 0, "frankie.png") 
    meme.text(
        20, 20, top_text.value, #adjust where the text drops on the image
        color=color.value,
        size=size.value, 
        font=font.value)
    meme.text(
        900, 550, bottom_text.value,
        color=color.value,
        size=size.value,
        font=font.value,
        )


# App -------------------

app = App("Mantis Tobaggan M.D.")

#Replace the values within the below double quotes with
#your own witty text
top_text = TextBox(app, "The Gang", command=draw_meme)
bottom_text = TextBox(app, "McPoyles", command=draw_meme)

#You can chage the options below, check out the official
#Python docs for other possible values
color = Combo(app,
              options=["black", "white", "red", "green", "blue", "orange"],
              command=draw_meme, selected="orange")

font = Combo(app,
             options=["times new roman", "verdana", "courier", "impact"],
             command=draw_meme)


#adjust the max and min values for the font size
#you want your text to look nice against the background
size = Slider(app, start=50, end=120, command=draw_meme)

meme = Drawing(app, width="fill", height="fill")

draw_meme()

app.display()
