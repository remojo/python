#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 15:40:52 2020


author: Miles Q. Llama IX
"""

#create a simple class to add items to a list
class Biggles():
    def __init__(self, list1):
        self.list1 = []
        self.list2 = list1
        
    def print_list(self):
        for i in self.list1:
            print('_>', i) #tag the output rows for the append list
    def print_list2(self):
        for i in self.list2:
            print('_*', i)  #tag original list
            
    def append_list(self, l):
        for i in l:
            self.list1.append(i)
            
biggles_list = Biggles(['Biggles', 'Johann Gambolputty..', 'Mr Neutron', 'Teddy Salad'])
biggles_list.append_list(['Coelocanth', 'The refeshment room at Bletchley', \
                     'Scott of the Antartic'])

biggles_list.print_list()
biggles_list.print_list2()

