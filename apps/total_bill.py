#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 23:04:21 2022

Basic function to calculate tax and 
surcharges
author: Miles Q. Llama IX
"""


def total(rate, nights):
    fee_total = (rate * 0.09) + (nights * 2)
    total_bill = (rate * nights) + fee_total
    return total_bill
