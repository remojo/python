class Insect:
    """A simple attempt to model an insect."""  
  
    def __init__(self, order, family, genus, species, author, year, 
                 flight, aquatic):
        """Initialize binomial nomenclature and age attributes."""
        self.order = order
        self.family = family
        self.genus = genus
        self.species = species
        self.author = author
        self.year = year
        self.flight = flight
        self.aquatic = aquatic
        self.wing_length = 0
        self.imago_length = 0

    def get_full_name(self):
        """Return the Order through Year"""
        long_name = f"{self.order} {self.family} {self.genus} {self.species} {self.author} {self.year}"
        return long_name #.title()
    def is_flight(self):
        """Simulate an insect that can fly as an adult."""
        if self.flight == 1:
            print(f"{self.genus} contains species capable of flying.")
        else:
            print(f"{self.genus} does not fly.")
    def is_aquatic(self):
        """Simulate an animal that spends part of it's life under water."""
        if self.aquatic == 1:
            print(f"{self.genus} contains aquatic species")
        else:
            print(f"{self.genus} does not appear to be aquatic")


    def read_wing_length(self):
        print(f"This specimen has a wing length of {self.wing_length} millimeters")

            
        """Update the wing length"""
    def update_w_l(self, wing_length):
        if wing_length >= self.wing_length:
            self.wing_length = wing_length
            print(f"New Maximum wing length!!")
        else:
            print(f"This value is below the current maximum value of {self.wing_length}")
            
            
    def read_imago_length(self):
        print(f"This specimen has a length of {self.imago_length} millimeters")
        
        
        
        """update the imago (adult insect) length"""
    def update_i_l(self, imago_length):
        if imago_length >= self.imago_length:
            self.imago_length = imago_length
            print(f"New Maximum Imago length!!")
        else:
            print(f"Imago length not the biggest")
            
            
            
specimen_01 = Insect('Coleoptera', 'Dytiscidae', 'Eretes', 'occidentalis', 
                     'Erichson', 1847, 0, 1)
specimen_02 = Insect('Lepidoptera', 'Papillionidae', 'Papillio', 'glaucus', 
                     'Linnaeus', 1978, 1, 0)

print(f">")
print(f">")
print(f">")

print(f"This speciman's full name is {specimen_01.get_full_name()}.")
print(f"{specimen_01.genus.title()} {specimen_01.species} was described in {specimen_01.year}.")
specimen_01.is_flight()
specimen_01.is_aquatic()
specimen_01.read_wing_length()
specimen_01.update_w_l(11)
specimen_01.read_wing_length()
print(f">")
print(f">")
print(f">")

print(f"This speciman's full name is {specimen_02.get_full_name()}.")
print(f"{specimen_02.genus.title()} {specimen_02.species} was described in {specimen_02.year}.")

specimen_02.is_flight()
specimen_02.is_aquatic()
print(f">")
print(f">")
print(f">")

print("This file created by the Great Semprini")

