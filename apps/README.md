This repository contains Python coding examples to create graphical desktop and command line applications.  

Depending on your workstation you may need to install additional software.
These apps were tested with Python version 3.7.6 and Linux kernel 5.8.0 generic.



The [source](http://lawsie.github.io/guizero) for this GUI example is from the Raspberry Pi official site.  Here we will use it to make rapid prototypes, get your project from concept to demo phase quickly. 


>
>$sudo apt install python3-tk
>

The meme generator examples use an an additional package created by the team at the Raspberry Pi Foundation called *guizero*. 

Use pip to install the package
>
>pip3 install quizero
>

Download the app to your local working folder, you should be able to launch it from the command line with
>
>$python3 noob-noob-meme-generator.py
>

You should be able to add your own text, background images, font and size.



![app example](/frankie.gif)



The file 'count_words_in_files.py' will help you count the number of words in each file in a directory.  This is useful for finding differences between backup drives, or monitoring if specific files have been modified on your server configuration. 
This file is a refactoring of code from Arash Shahvar. 
Online Udemy.com course "Django & Python: Complete Bundle



The class_combine_lists.py app is a basic example of how to wrap functions into a Python class structure.  Add the items in the first list to the second and save.  




Last update 21-Aug-2021
