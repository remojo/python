#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 22:50:22 2020
This file is a refactoring of code from Arash Shahvar. 
Online Udemy.com course "Django & Python: Complete Bundle"
semprini
author: Miles Q. Llama IX
"""
print('Count the number of words in multiple files at once.\n')
print('>')
print('>')
print('>')

def count_words(filename):
    """Count the number of words in multiple files at once."""
    try:
        with open(filename) as file:
            file = file.read()
    except FileNotFoundError:
        msg = 'Sorry, the file {} does not exist\n'.format(filename)
        print(msg)
    else:
        print('Counting the number of words in \"{}\", please wait...'.format(filename))
        word = file.split()
        num_word = len(word)
        print('The file \"{}\" has about {} words\n'.format(filename, num_word))

"""Read specific files in a local directory."""
#filename = 'finnegans_wake.txt'
#filenames = ['../data/raw/finnegans_wake.txt',
#             '../data/raw/micah2.txt',
#             '../data/raw/micah4.txt',
#             '../data/raw/fakey.txt',
#             '../data/raw/grail_credits.txt',
#             '../data/raw/grail_credits_spaces.txt',
#             ]
"""Read every text file in the directory."""
filename = ['../data/raw/*.txt']
for filename in filenames:
    count_words(filename)

        
