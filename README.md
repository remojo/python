# Micah's Python Sources ######

The enclosed folders are my personal notes and sources that I use to craft software with the Python programming language.  
In my sources.txt file you will find a list of references by author name, followed
by links to oneline courses. 


The majority of the content here is core competencies and simple applications. The apps folder contains examples of each to show how to experiment and mix them together.  These apps can be used with a basic installation of Python on your computer.  Source code for these applications comes from books and online courses that I have completed, authors are cited in the sources.txt file.


The jupyter folder contains examples of Python code that can be run interactively within your favorite web browser. You will need to [install](https://ipython.org/) additional software if you want to operate the notebook on your local device.  Code is arranged in blocks that can be triggered individually as you implement each step of your business requirements.  If all of your steps work, you can compile these blocks into one at the bottom of the notebook.  



The ml folder contains basic research models for scikit-learn which is a Python module for advanced analysis.  Machine learning (ML) algorithms can be used to create applications that automatically improve themselves based on data and experience. 

I start with a posionous mushroom example I found on the data site Kaggle.  The TPS generator spreadsheet can be used to generate a test categorical data set, create a pattern and see if your model can detect it. Most of this code came from pluralsight courses by Janani Ravi, use the TPS reports to replace her wine data set.  

The TPS report generator can also be used to add un-needed columns which will require extra steps to remove prior to importing the dataframe.  This is to mimic real world production environments where you do not have control over the input file and have to perform cleaning steps.  



Thanks for visiting my coding repositories, I hope you find something useful!
>Micah Thomas


