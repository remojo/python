##Welcome to my Python page for Jupyter (IPython) notebooks.
===========================================================
Background
Jupyter notebooks are an easy way to share coding research, simply download and experiment in your own development environment.  
These notebooks can be viewed as a webpage by pasting the URL into nbviewer.jupyer.org.  You do not need to install Python to view the notebooks with this option. 

Example files are in the data directory, you can download them individually if you do not clone the entire repository.  Program files with the extension ".py" can be run with just the basic Python installation, no extra packages needed unless noted otherwise.

My notebooks are grouped by Python modules.


1.    This notebook covers the basic examples of the Python Language and how to use Jupyter Notebooks 00_mt_core_types_loops.ipynb


2.    Regular expressions are covered in detail in this notebook, most of the code is from Al Swigart's 'Automate the Boring Stuff' series   00_mt_py_regular_expressions.ipynb



3.   Numerical Python (NumPy) provides the core of most other advanced Scientific 
Python modules, this notebook covers basic array building and analysis. 00_mt_numpy_codebase.ipynb


4.    This notebook contains further coding experiments, especially with FOR an WHILE loops.
Generators and iterators can be constructed with these core examples.  00_mt_py_functions_tests_iterations.ipynb



Two notebooks for building analysis applications with the *pandas* library, the first one covers the basics of creating common dataframes (tabular rows and columns) and series (a really long line).  The second example shows how to import a variety of files, do a simple transformation or two, then export to a variety of outputs.


..*00_mt_pd_dataframes_series.ipynb


..*01_mt_pd_IO_save.ipynb



The source code for this notebook came from a training module by author Janani Ravi, titled 'Visualizing Statistical Data Using Seaborn' on pluralsight.com. It contains basic examples for plotting columns after importing CSV files. e.g. a failed bank list, a TPS report, and bike path usage graph.

+01_mt_pd_csv_read.ipynb




The data folder contains coconuts, parrot_archive, Eye_of_Parrot and compressed_parrot_whizzo which are example output files from unit tests.



##Use Jupyter Notebooks for your everyday tasks.
===========================================================

###Audience.
A large data set can quickly outgrow the best desktop spreadsheet applications. Advanced spreadsheet users should find the enclosed notebooks easy to modify and adapt to their particular buisness requirement or scientific calculation. 

The pandas library allows you to quickly build simple applications to perform analysis and automation scripts.  The process of extracting data, transforming it and loading into another database is the core of most operations.  Pandas functions can replace having to write functions or loops in your app which will reduce the overall lines of code.  In addition you code will be easier to explain to non-experts and can be quickly modified to adapt to a similar requirement.


exit
 
